<?php

use App\Http\Controllers\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Email authentication

Route::get('/publish', function(){
    Redis::publish('assets', json_encode(['cache' => 'clear']));
});

Auth::routes(['verify' => true]);

Route::get('/', function () { return view('welcome'); });

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/uploadasset', 'UploadAssetController@index')->name('uploadasset');

Route::get('/upload_asset', 'singlePageController@upload_asset')->name('upload_asset');

/* Most recent reactive form */
Route::get('/upload_your_asset', 'UploadAssetController@index_new')->name('upload_your_asset');

Route::get('/user_dashboard', 'singlePageController@dashboard')->name('user_dashboard');

Route::get('/{userid}/num_assets', 'assetOwnerController@asset_count_per_owner');

Route::get('/assetOwnerProfiles', 'assetOwnerController@index');

Route::get('/{userid}/list_assets', 'assetOwnerController@list_assets');

// Display result after uploading asset
Route::post('/upload_asset_after_posting_form', 'UploadAssetController@store')->name('upload_asset_after_posting_form');
// Upload an asset using form as a Vue component
Route::post('/submit', 'UploadAssetController@store')->name('submit');



