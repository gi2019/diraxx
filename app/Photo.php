<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $guarded = []; // disable mass-assignment protection

    protected $fillable = ['asset_id', 'file_name_and_path'];


    public function asset(){
        return $this->belongsTo('App\Asset','asset_id');
    }
}
