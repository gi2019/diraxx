<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class assetOwnerProfile extends Model
{
    protected $guarded = []; // disable mass-assignment protection

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function asset() {
        return $this->hasMany(Asset::class);
    }
    // method that returns number of assets user has. according user id
    public function asset_count_per_owner($id){
        $num_of_assets_per_this_user = DB::table('assets')
                                            -> where('asset_owner_profile_id',$id)
                                            -> count('id');
        return $num_of_assets_per_this_user;
    }
    // method that returns array of assets id's,address of a user. according user id.
    public function asset_id_and_address_per_owner($id){
        $result = DB::table('assets')
            -> where('asset_owner_profile_id',$id)
            ->select('id','address')
            ->get();
        $array = $result->toArray();
        return $array;
    }


}
