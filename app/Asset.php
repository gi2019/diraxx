<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{

    protected $guarded = []; // disable mass-assignment protection

    protected $fillable = ['country_code', 'state_code', 'address', 'asset_owner_profile_id', 'isResidential', 'isOccupied', 'hasParking'
    , 'num_rooms', 'rating', 'locality', 'asset_area', 'floor_number', 'property_tax', 'state_code', 'area_code', 'unit_of_measure',
        'asset_description', 'price', 'price_currency', 'tax_currency', 'hasPorch'];

    public function photos(){
        return $this->hasMany('App\Photos','asset_id');
    }

    public function assetOwner() {
        return $this->belongsTo(assetOwnerProfile::class);
    }
}
