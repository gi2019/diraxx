<?php

namespace App\Http\Controllers;

use App\assetOwnerProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class assetOwnerController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    public function index()
    {
        return assetOwnerProfile::all();
    }

    public function asset_count_per_owner($id) {
        if ($id == auth()->user()->id) {
            $num_of_assets_per_this_user = DB::table('assets')
                ->where('asset_owner_profile_id', $id)
                ->count('id');
            return $num_of_assets_per_this_user;
        } else
            return abort(403);
    }

    public function list_assets($id) {
        if ($id == auth()->user()->id) {
            return DB::table('assets')
                ->where('asset_owner_profile_id', $id)
                ->get()->toArray();
        } else
            return abort(403);
    }

}
