<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SinglePageController extends Controller
{
    public function index() {
        return view('app');
    }

    public function upload_asset() {
        return view('home');
    }

    public function dashboard() {
        return view('home');
    }
}
