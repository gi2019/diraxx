<?php

namespace App\Http\Controllers;

use App\Asset;
use Illuminate\Http\Request;

class UploadAssetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('uploadAsset');
    }

    public function index_new()
    {
        return view('upload_your_asset');
    }

    public function create(array $data)
    {
        return Asset::create([
            'address' => $data['asset_address'],
            'isResidential' => $data['rental']
        ]);
    }

    public function store(Request $request) {
        $this->validate(
            request(), [
                'country' => 'required',
            'locality' => 'required|min:2|max:35|',
            'address' => 'required|min:5|max:45|',
            'price' => 'required|integer'
        ]);
        /*
         * cleaning locality and address values from user before putting in db
         */
        $clean_locality = str_replace(array(':', '-', '/', '*', '=', '?', '$', '&', '<', '>'), '', $request->locality);
        $clean_address = str_replace(array(':', '-', '/', '*', '=', '?', '$', '&', '<', '>'), '', $request->address);
        $clean_asset_description = str_replace(array(':', '-', '/', '*', '=', '?', '$', '&', '<', '>'), '', $request->asset_description);
        $asset = new Asset(
            [
                'country_code' => $request->country,
                'state_code' => $request->state_code,
                'area_code' => (empty($request->area_code) ? 0 : $request->area_code),
                'locality' => $clean_locality,
                'address' => $clean_address,
                'num_rooms' => $request->num_rooms,
                'asset_area' => $request->asset_area,
                'floor_number' => $request->floor_number,
                'property_tax' => $request->property_tax,
                'asset_owner_profile_id' => $request->asset_owner_profile_id,
                'isResidential' => (empty($request->isResidential) ? 0 : $request->isResidential),
              //  'isOccupied' => (empty($request->isOccupied) ? 0 : $request->isOccupied),
                'hasParking' => (empty($request->hasParking) ? 0 : $request->hasParking),
                'rating' => 3.5,
                'unit_of_measure' => $request->unit_of_measure,
                'asset_description' => $clean_asset_description,
                'price' => $request->price,
                'price_currency' => $request->price_currency,
                'tax_currency' => $request->price_currency,
                'hasPorch' => (empty($request->hasPorch) ? 0 : $request->hasPorch),
                'isFurnished' => (empty($request->isFurnished) ? 0 : $request->isFurnished),
                'entrance_date' => $request->entrance_date
            ]);
        $asset->save();
        // moving uploaded file array to public\img
        $path = public_path() . '/img/assets';
        $files_array = $request->file('photos');
        if (!empty($files_array)) {
            foreach ($files_array as $file) {
                /*check via private method: check_if_file_is_an_image method, if the file
                is an image of accepted format and size of it <= 300kb. If yes move to directory on server
                plus put name in table.
                if no, continue to next file
                */
                if ($this->check_if_file_is_an_image($file->getClientOriginalName())) {
                    if (filesize($file) <= 300000) {
                        $file_original_name = $file->getClientOriginalName();
                        $file_new_diraxx_name = time() . '_asset_' . $asset->id . '_' . $file_original_name;
                        $file->move($path, $file_new_diraxx_name);
                        // creating new $photo object to store photo data in photos table;
                        $photo = new Photo(
                            [
                                'asset_id' => $asset->id,
                                'file_name_and_path' => url('/img/assets') . '/' . $file_new_diraxx_name
                            ]
                        );
                        $photo->save();
                    } else {
                        continue;
                    }
                } else {
                    continue;
                }
            }
        } else {
            // Do nothing
        }
        return redirect('/home');
    }

    /* This private method receives a file name.
       If the letters after the point, in file name, are either jpg, jpeg, or bnp,
       function returns true. else returns flse
    */
    private function check_if_file_is_an_image($file_name)
    {
        $dot_pos_in_file_name = strpos($file_name, '.'); // find position in string of "."
        // check if first to letters after . are JP (can be jpeg, can be jpeg) - ok
        $first_two_letters_after_dot = substr($file_name, $dot_pos_in_file_name + 1, 2);
        if ($first_two_letters_after_dot === 'jp') {
            // check if next letter is 'g'. if yes return true.
            // if no check if its e + g if none of the above return false
            if ((substr($file_name, $dot_pos_in_file_name + 3, 1) === 'g')
                || (substr($file_name, $dot_pos_in_file_name + 3, 2) === 'eg')) {
                return true;
            } else {
                return false;
            }
        } else {
            $last_letter_in_file_name = substr($file_name, strlen($file_name) - 1, 1);
            // check if 3 last letters are bnp
            if (($first_two_letters_after_dot === 'bn') && ($last_letter_in_file_name === "p")) {
                return true;
            } else {
                // check if 3 last letters are png
                if (($first_two_letters_after_dot === 'pn') && ($last_letter_in_file_name === "g")) {
                    return true;
                } else {
                    // check if 4 last letters are jfif
                    if (($first_two_letters_after_dot === 'jf') && (substr($file_name, $dot_pos_in_file_name + 3, 2) === 'if')) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
    }

    public function storeWithVue(Request $request) {
        $this->validate(
            request(), [
            'locality' => 'required|min:2|max:35|',
            'address' => 'required|min:5|max:45|',
        ]);
        /*
         * cleaning locality and address values from user before putting in db
         */
        $clean_locality = str_replace(array(':', '-', '/', '*', '=', '?', '$', '&', '<', '>'), '', $request->locality);
        $clean_address = str_replace(array(':', '-', '/', '*', '=', '?', '$', '&', '<', '>'), '', $request->address);
        $clean_asset_description = str_replace(array(':', '-', '/', '*', '=', '?', '$', '&', '<', '>'), '', $request->asset_description);
        $asset = new Asset(
            [
                'country_code' => $request->country_code,
                'state_code' => $request->state_code,
                'area_code' => $request->area_code,
                'locality' => $clean_locality,
                'address' => $clean_address,
                'num_rooms' => $request->num_rooms,
                'asset_area' => $request->asset_area,
                'floor_number' => $request->floor_number,
                'property_tax' => $request->property_tax,
                'asset_owner_profile_id' => $request->asset_owner_profile_id,
                'isResidential' => (empty($request->isResidential) ? 0 : $request->isResidential),
                'isOccupied' => (empty($request->isOccupied) ? 0 : $request->isOccupied),
                'hasParking' => (empty($request->hasParking) ? 0 : $request->hasParking),
                'rating' => $request->rating,
                'unit_of_measure' => $request->unit_of_measure,
                'asset_description' => $clean_asset_description,
                'price' => $request->price,
                'price_currency' => $request->price_currency,
                'tax_currency' => $request->tax_currency,
                'hasPorch' => (empty($request->hasPorch) ? 0 : $request->hasPorch)
            ]);
        $asset->save();

        // moving uploaded file array to public\img
        $path = public_path() . '/img/assets';
        $files_array = $request->file('photos');
        if (!empty($files_array)) {
            foreach ($files_array as $file) {
                /*check via private method: check_if_file_is_an_image method, if the file
                is an image of accepted format and size of it <= 300kb. If yes move to directory on server
                plus put name in table.
                if no, continue to next file
                */
                if ($this->check_if_file_is_an_image($file->getClientOriginalName())) {
                    if (filesize($file) <= 300000) {
                        $file_original_name = $file->getClientOriginalName();
                        $file_new_diraxx_name = time() . '_asset_' . $asset->id . '_' . $file_original_name;
                        $file->move($path, $file_new_diraxx_name);
                        // creating new $photo object to store photo data in photos table;
                        $photo = new Photo(
                            [
                                'asset_id' => $asset->id,
                                'file_name_and_path' => url('/img/assets') . '/' . $file_new_diraxx_name
                            ]
                        );
                        $photo->save();
                    } else {
                        continue;
                    }
                } else {
                    continue;
                }
            }
        } else {
            // Do nothing
        }
        return redirect('/home');
    }

}
