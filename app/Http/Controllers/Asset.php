<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Asset extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'verified']);
    }

    //show asset page for specific asset
    public function index($asset_id) {
        return view('Asset')->with('asset_id',$asset_id);
    }
}
