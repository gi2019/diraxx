<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RenterProfile extends Model
{
    protected $guarded = []; // disable mass-assignment protection

    protected $attributes = [ // Set default values to following columns
        'isFamily' => '0',
        'isStudent' => '0',
        'isBusiness' => '0'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
