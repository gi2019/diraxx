<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFourFinalColumnsToAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->tinyInteger('unit_of_measure')->unsigned(); // values : 1 -> square feet , 2 -> square meter.
            $table->string('asset_description'); // free text.
            $table->tinyInteger('currency')->unsigned(); // 1 - us dollar, 2 israeli new shekel, 3 - euro.
            $table->tinyInteger('hasPorch')->unsigned(); // null , 1 - has porch.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropColumn('unit_of_measure');
            $table->dropColumn('asset_description');
            $table->dropColumn('currency');
            $table->dropColumn('hasPorch');
        });
    }
}
