<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Doctrine\DBAL\Driver\PDOMySql\Driver;

class CreateAssetOwnerView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::connection()->getPdo()->exec("CREATE VIEW AssetsPerOwners AS 
        SELECT asset_owner_profiles.user_id AS user_id, asset_owner_profiles.name AS Name, COUNT(*) AS NUM_ASSETS FROM asset_owner_profiles, assets
        WHERE assets.asset_owner_profile_id = asset_owner_profiles.user_id
        GROUP BY asset_owner_profiles.user_id");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::connection()->getPdo()->exec("DROP VIEW AssetsPerOwners;");
    }
}
