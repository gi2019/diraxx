<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToAssetOwnerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_owner_profiles', function (Blueprint $table) {
            $table->index('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_owner_profiles', function (Blueprint $table) {
            $table->dropForeign('asset_owner_profiles_user_id_foreign');
            $table->dropIndex('asset_owner_profiles_user_id_index');
            $table->dropColumn('user_id');
        });
    }
}
