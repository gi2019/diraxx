import Vue from 'vue';
import VueRouter from 'vue-router';

import Asset from '@/js/components/Asset';
import Home from '@/js/components/Home';
import StatisticsComponent from "./components/StatisticsComponent";
import ImageItem from "./components/ImageItem";
import App from "./views/App";
import UploadAssetFormFull from "./components/UploadAssetFormFull";


Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/upload_asset',
            name: 'upload_asset',
            component: UploadAssetFormFull,
            props: true
        },
        {
            path: '/user_dashboard',
            name: 'user_dashboard',
            component: StatisticsComponent
        },
        {
            path: '/image_item',
            name: 'image_item',
            component: ImageItem
        }
    ]
});

export default router;