import './bootstrap';
import Vue from 'vue';
import Vuetify from 'vuetify';

import Select from '@/js/views/countryStateSelect';
import TrendChart from "vue-trend-chart";

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import {Datepicker, Timepicker, DatetimePicker} from '@livelybone/vue-datepicker';

import GoogleAutocomplete from 'google-autocomplete-vue';

library.add(faUserSecret);


Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('Water', {
    // camelCase in JavaScript
    props: ['btnName'],
    template: '<v-btn>{{ btnName }}</v-btn>'
});

//Vue.component('google-autocomplete', GoogleAutocomplete);

Vue.component('country-state-province-switch', require('./components/countryStateProvinceSwitch').default);

Vue.component('upload-asset-form', require('./components/uploadAssetForm').default);

Vue.component('upload-asset-form-full', require('./components/UploadAssetFormFull').default);

Vue.component('SearchComponent', {

});

Vue.component('Notif', require('./components/Notif').default);

Vue.component('date-comp', require('./components/DatePicker').default);


Vue.config.productionTip = false;

Vue.use(Vuetify);
Vue.use(TrendChart);


/*const app = new Vue({
    el: '#app',
    router: Routes,
    vuetify: new Vuetify(),
    render(h) {
        return h(App, {
            props: {
                userid: this.$el.attributes.userid.value
            }
        })
    }
});*/

const app = new Vue({
   el: '#countryStateSelect'
});

//export default searchbar;
//export default app;
