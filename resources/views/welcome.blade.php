@extends('layouts.app')

@section('content')
    <div class="row row2">
        <div class="col-md-10 offset-1">
            @auth
                @if(Auth::user()->hasVerifiedEmail())
                    <div class="dashboard font-source-sans-pro">
                        <a href="/home">
                            go to dashboard
                            <i class="fas fa-tachometer-alt"></i>
                        </a>
                    </div>
                @else

                @endif
            @endauth
        </div>
    </div>
    <div class="row row3">
        <div class="col-5">
            <img src="{{asset('img/snale_wo_slo.png')}}" alt="" class="img2">
        </div>
        <div class="col-7">
            <h1>
                Diraxx
            </h1>
            <div class="list1">
                <ul>
                    <li>
                        310,852 followers <i class="fas fa-circle"></i>
                    </li>
                    <li>
                        <span class="number">0</span>
                        Following
                        <i class="fas fa-circle"></i>
                    </li>
                    <li>
                        Diraxx.com
                    </li>
                </ul>
            </div>
            <!-- Snale animation -->
            <div class="snale_animate">
                <?php define('DIR1', realpath(__DIR__ . '/../../public'));
                require_once DIR1 . 'snl_svg_normal.php';
                require_once DIR1 . 'snl_svg_blink.php';
                require_once DIR1 . 'snl_svg_got_idea.php';
                require_once DIR1 . 'snl_svg_runs.php';
                require_once DIR1 . 'house_of_snale_svg.php';
                require_once DIR1 . 'house_of_snale_svg_open_door.php';
                require_once DIR1 . 'snale_enters_his_house.php';
                ?>
            </div>
            <div class="snale_ground">

            </div>
        </div>
    </div>
    <div class="row row4">
        <div class="col-6">
            <h1>
                <span> Diraxx's </span> best boards
            </h1>
        </div>
    </div>
    <div class="row row5">
        <div class="col col1">
            <a href="">
                <ul>
                    <li>
                        <img src="{{asset('img/welcome_row5_img1.jpg')}}" alt="" class="img img1">
                    </li>
                    <li>
                        <img src="{{asset('img/welcome_row5_img2.jpg')}}" alt="" class="img img2">
                    </li>
                    <li>
                        <img src="{{asset('img/welcome_row5_img3.jpg')}}" alt="" class="img img3">
                    </li>
                    <li>
                        <img src="{{asset('img/welcome_row5_img4.jpg')}}" alt="" class="img img4">
                    </li>
                </ul>
            </a>
        </div>
        <div class="col col2">
            <a href="">
                <ul>
                    <li>
                        <img src="{{asset('img/welcome_row5_img5.jpg')}}" alt="" class="img img1">
                    </li>
                    <li>
                        <img src="{{asset('img/welcome_row5_img6.jpg')}}" alt="" class="img img2">
                    </li>
                    <li>
                        <img src="{{asset('img/welcome_row5_img7.jpg')}}" alt="" class="img img3">
                    </li>
                    <li>
                        <img src="{{asset('img/welcome_row5_img8.jpg')}}" alt="" class="img img4">
                    </li>
                </ul>
            </a>
        </div>
        <div class="col">
            <a href="">
                <ul>
                    <li>
                        <img src="{{asset('img/welcome_row5_img9.jpg')}}" alt="" class="img img1">
                    </li>
                    <li>
                        <img src="{{asset('img/welcome_row5_img10.jpg')}}" alt="" class="img img2">
                    </li>
                    <li>
                        <img src="{{asset('img/welcome_row5_img11.jpg')}}" alt="" class="img img3">
                    </li>
                    <li>
                        <img src="{{asset('img/welcome_row5_img12.jpg')}}" alt="" class="img img4">
                    </li>
                </ul>
            </a>
        </div>
        <div class="col">
            <a href="">
                <ul>
                    <li>
                        <img src="{{asset('img/welcome_row5_img13.jpg')}}" alt="" class="img img1">
                    </li>
                    <li>
                        <img src="{{asset('img/welcome_row5_img14.jpg')}}" alt="" class="img img2">
                    </li>
                    <li>
                        <img src="{{asset('img/welcome_row5_img15.jpg')}}" alt="" class="img img3">
                    </li>
                    <li>
                        <img src="{{asset('img/welcome_row5_img16.jpg')}}" alt="" class="img img4">
                    </li>
                </ul>
            </a>
        </div>
    </div>
@stop

@section('footer')

@stop