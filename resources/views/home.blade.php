<?php
use App\assetOwnerProfile;
?>
@extends('layouts.app')

@section('content')
    <div class="container home">
        <div class="row rowh1 justify-content-center">
            <div class="col-md-6">
                <!-- empty for now -->
            </div>
            <div class="col-md-6">
                <ul>
                    <li class="dashboard">
                        <i class="fas fa-tachometer-alt"></i>
                    </li>
                    <li>
                        <a href="">
                            <i class="fas fa-th"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="fas fa-download"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="fas fa-tasks"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="fas fa-user"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card card-home">
                    <div class="card-header">
                        <!-- Put in data attribute if a user is_asset_owner. 0 - Not ans asset owner, 1 He is an asset owner. -->
                        <div class="row rowh2" data-role= <?php echo Auth::user()->roles;?>>
                            <button class="col col1">
                                <i class="fas fa-home"></i>
                            </button>
                            <div class="col col2">
                                <i class="fas fa-chart-area"></i>
                            </div>
                            <div class="col col3">
                                <i class="fas fa-camera"></i>
                            </div>
                            <div class="col col4">
                                <i class="far fa-money-bill-alt"></i>
                            </div>
                        </div>
                        <div class="row rowh3">
                            <?php
                            // array of blue shades of asset icons
                            $house_icon_color_array = ['#99c2ff', '#80b3ff', '#66a3ff', '#4d94ff', '#3385ff', '#1a75ff', '#005ce6', '#003d99', '#003380'];
                            /* Display of dashboard with assets.
                            we need to query DB to display amounts */
                            $assetOwnerProfile = new assetOwnerProfile();
                            $number_of_assets = $assetOwnerProfile->asset_count_per_owner(Auth::user()->id);
                            // retrieving asset's id
                            $array_of_objects_consisting_of_assets_id_number = $assetOwnerProfile->asset_id_and_address_per_owner(Auth::user()->id);
                            // set counter that will be used to retrieve each asset id from array above.
                            $counter = 0;
                            // calculate number of assets div 5. module 5 in order to see numbers of <ul>
                            $number_of_ul_elements = (int)(($number_of_assets) / 5);
                            if ($number_of_assets % 5 > 0) {
                                $number_of_ul_elements++;
                            }
                            for ($i = 0; $i < $number_of_ul_elements; $i++) {
                            ?>
                            <ul class="number_of_assets">
                                <?php
                                for ($u = 0;$u < min(5, $number_of_assets, $number_of_assets - ($i * 5));$u++){
                                $house_icon_color = $house_icon_color_array[rand(0, 8)];
                                ?>
                                <li>
                                    <?php $asset_id = $array_of_objects_consisting_of_assets_id_number[$counter]->id;
                                    $url = '/asset/'.$asset_id;
                                    ?>
                                    <a href="<?php echo App::make('url')->to($url); ?>">
                                        <i class="far fa-building"
                                           style="color:<?php echo $house_icon_color;?>;font-size: 40px"></i>
                                    </a>
                                </li>
                                <div class="address">
                                    <?php echo substr($array_of_objects_consisting_of_assets_id_number[$counter]->address,0,5);?>
                                </div>
                                <?php
                                $counter++;
                                }
                                ?>
                            </ul>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        You are logged in {{Auth::user()->name}}!
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var role = $(".rowh2").data("role");
            if (1 === role) {
                // he is not an asset owner
                $(".col1").prop('disabled', true);
            } else {
                // he is an asset owner this user
                $(".col1").prop('disabled', false);
            }
            // re route to upload asset
            $(".col1").click(function () {
                window.location.href = '/uploadasset';
            });

        });
    </script>
@endsection

