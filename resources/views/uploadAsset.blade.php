@extends('layouts.app')

@section('content')
    <div class="container-fluid upload">
        <div class="row rowu1">
            <div class="col-md-8 offset-2">
                <h1>
                    Upload your asset
                </h1>
            </div>
            <div class="dashboard font-source-sans-pro">
                <a href="/home">
                    go to dashboard
                    <i class="fas fa-tachometer-alt"></i>
                </a>
            </div>
        </div>
        <div class="row rowu2">
            <div class="col-md-10 offset-1">
                <form action="{{route('upload_asset_after_posting_form')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-3">
                            <!-- Country select box -->
                            <div class="form-group">
                                <label for="country_code">Select Country:</label>
                                <select class="form-control" id="country_code" name="country_code"
                                        style="max-width: 120px;">
                                    <option value="1">USA</option>
                                    <option value="2">Canada</option>
                                    <option value="3">Israel</option>
                                </select>
                            </div>
                            <!-- -->
                            <!-- State select box -->
                            <div class="form-group">
                                <label for="state_code">Select State (Not for Israel):</label>
                                <select class="form-control" id="state_code" name="state_code"
                                        style="max-width: 120px;">
                                    <option value="99"></option> <!-- Not Relevant for this asset -->
                                    <option value="1">New York</option>
                                    <option value="2">New Jersey</option>
                                    <option value="3">New Mexico</option>
                                </select>
                            </div>
                            <!-- -->
                            <!-- Area select box -->
                            <div class="form-group">
                                <label for="area_code">Select Area (Israel only):</label>
                                <select class="form-control" id="area_code" name="area_code" style="max-width: 120px;">
                                    <option value="99"></option> <!-- Not Relevant for this asset -->
                                    <option value="1">Gush Dan</option>
                                    <option value="2">Galil</option>
                                    <option value="3">Negev</option>
                                </select>
                            </div>
                            <!-- -->
                            <!-- Locality -->
                            <div class="form-group">
                                <label for="locality">Locality:</label>
                                <input type="text" class="form-control" id="locality"
                                       name="locality" style="max-width: 200px">
                                <span class="locality_message">
                                    required, at least 2 characters
                                </span>
                            </div>
                            <!-- -->
                            <button type="submit" class="btn btn-success" id="upload_asset_submit_btn"
                                    style="margin-top: 10px;background: #7ED19E!important;border: #7ED19E!important;">
                                Submit
                            </button>
                        </div>
                        <div class="col-5">
                            <!-- Address -->
                            <div class="form-group">
                                <label for="address">Address:</label>
                                <input type="text" class="form-control" id="address" name="address">
                                <span class="address_message">
                                    required, at least 5 characters
                                </span>
                            </div>
                            <!-- -->
                            <!-- Number of rooms -->
                            <div class="form-group">
                                <label for="num_rooms">Number of rooms:</label>
                                <input type="number"
                                       id="num_rooms"
                                       name="num_rooms"
                                       step="0.5"
                                       min="1"
                                       max="7"
                                       value="3.5"
                                       class="form-control"
                                       style="max-width: 80px;text-align: center"
                                >
                            </div>
                            <!-- -->
                            <div class="row" style="margin-top: -10px">
                                <div class="col-6">
                                    <div class="form-group">
                                        <!-- Asset area -->
                                        <label for="asset_area">Asset area:</label>
                                        <input type="number"
                                               id="asset_area"
                                               name="asset_area"
                                               step="0.5"
                                               min="30"
                                               max="1000"
                                               value="30"
                                               class="form-control"
                                               style="max-width: 80px;text-align: center"
                                        >
                                        <!-- -->
                                    </div>
                                </div>
                                <div class="col-6" style="margin-left:-100px;">
                                    <label for="unit_of_measure" class="label1">Unit of measure:</label>
                                    <select class="select1 select2" name="unit_of_measure" id="unit_of_measure">
                                        <option value="1">square feet</option>
                                        <option value="2">square meter</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="margin-top: -18px">
                                <div class="col-12">
                                    <!-- Free asset description -->
                                    <label for="asset_description">Asset description:</label>
                                    <textarea placeholder=""
                                              rows="20" name="asset_description"
                                              id="asset_description"
                                              cols="40"
                                              class="ui-autocomplete-input"
                                              autocomplete="off"
                                              role="textbox"
                                              aria-autocomplete="list"
                                              aria-haspopup="true"></textarea>
                                    <!-- -->
                                    <!-- Max asset description length  -->
                                    <div id="asset_description" class="asset_description">
                                        <p class="font-source-sans-pro">
                                            Please limit the description to 420 characters
                                        </p>
                                    </div>
                                    <!-- -->
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <!-- Floor number -->
                            <div class="form-group">
                                <label for="floor_number">Floor number:</label>
                                <input type="number"
                                       id="floor_number"
                                       name="floor_number"
                                       step="1"
                                       min="0"
                                       max="100"
                                       value="0"
                                       class="form-control"
                                       style="max-width: 95px;text-align: center"
                                >
                            </div>
                            <!-- Price  -->
                            <!-- prise_currency -->
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="price">Price:</label>
                                        <input type="number"
                                               id="price"
                                               name="price"
                                               step="50"
                                               min="100"
                                               max="9000"
                                               value="100"
                                               class="form-control"
                                               style="max-width: 80px;text-align: center"
                                        >
                                    </div>
                                </div>
                                <div class="col-6" style="margin-left: -50px;">
                                    <label for="price_currency" class="label1">
                                        Currency:
                                    </label>
                                    <select class="select1 select2" name="price_currency" id="price_currency">
                                        <option value="1">
                                            &#36;
                                        </option>
                                        <option value="2">
                                            &#8362;
                                        </option>
                                        <option value="3">
                                            &#8364;
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <!-- -->
                            <!-- Property_tax -->
                            <!-- tax_currency -->
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="property_tax">Property tax:</label>
                                        <input type="number"
                                               id="property_tax"
                                               name="property_tax"
                                               step="10"
                                               min="100"
                                               max="5000"
                                               value="100"
                                               class="form-control"
                                               style="max-width: 80px;text-align: center"
                                        >
                                    </div>
                                </div>
                                <div class="col-6" style="margin-left: -50px;">
                                    <label for="tax_currency" class="label1">
                                        Currency:
                                    </label>
                                    <select class="select1 select2" name="tax_currency" id="tax_currency">
                                        <option value="1">
                                            &#36;
                                        </option>
                                        <option value="2">
                                            &#8362;
                                        </option>
                                        <option value="3">
                                            &#8364;
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <!-- -->
                            <!-- Asset_owner_profile_id -->
                            <input type="hidden" id="asset_owner_profile_id" name="asset_owner_profile_id"
                                   value="{{Auth::user()->id}}">
                            <!-- isResidential -->
                            {{--<div class="form-check">--}}
                                {{--<input class="form-check-input" type="checkbox" value="1" id="isResidential"--}}
                                       {{--name="isResidential">--}}
                                {{--<label class="form-check-label" for="isResidential">--}}
                                    {{--residential ?--}}
                                {{--</label>--}}
                            {{--</div>--}}
                            <!-- -->
                            <!-- Available immediately -->
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1" id="available_immediately"
                                       name="available_immediately">
                                <label class="form-check-label" for="available_immediately">
                                    Available immediately ?
                                </label>
                            </div>
                            <!-- -->
                            <!-- hasPorch -->
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1" id="hasPorch"
                                       name="hasPorch">
                                <label class="form-check-label" for="hasPorch">
                                    has Porch ?
                                </label>
                            </div>
                            <!-- -->
                            <!-- hasParking -->
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1" id="hasParking"
                                       name="hasParking">
                                <label class="form-check-label" for="hasParking">
                                    has private parking ?
                                </label>
                            </div>
                            <!-- -->
                            <!-- Rating -->
                            <div class="form-group">
                                <input type="hidden"
                                       id="rating"
                                       name="rating"
                                       value="3.5"
                                >
                            </div>
                            <!-- Upload pictures -->
                            <div class="custom-file" style="max-width: 190px;margin: 15px 0;">
                                <input type="file" class="custom-file-input" id="photos" name="photos[]" multiple>
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                            <!-- -->
                            <!-- Upload pictures message box  -->
                            <div id="picture_message" class="picture_message">
                                <p class="font-source-sans-pro">
                                    Please upload only jpg's, jpeg's, png's, bmp's, jfif's and files which wiegh less
                                    than 300KB
                                </p>
                            </div>
                            <!-- -->
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Validation Error masseges -->
        @if(count($errors))
            <div class="form-group">
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        <!-- Snale animation + overlay -->
        <?php define('DIR1', realpath(__DIR__ . '/../../public'));
        require_once DIR1 . 'snl_svg_normal_spinner.php';
        ?>
        <!-- Animation transition overlay -->
        <div class="overlay"></div>
        <!-- -->
    </div>
@endsection
