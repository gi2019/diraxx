<?php
use App\Asset;
use App\Photo;
?>
@extends('layouts.app')
<?php
$asset_obj = Asset::where('id', $asset_id)->get();
?>
@section('content')
    <div class="container">
        <div class="row rows1">
            <div class="col-4">
                <div class="circ1">
                    <p class="circText1">
                        <?php echo substr($asset_obj[0]->address, 0, 10); ?>
                    </p>
                    <?php
                    $locality = $asset_obj[0]->locality;
                    $locality_length = strlen($locality);
                    if($locality_length > 10){
                    ?>
                    <p class="circText2" style="margin: 20px 0 0 17%;">
                        {{$locality}}
                    </p>
                    <?php
                    } else{
                    ?>
                    <p class="circText2">
                        {{$locality}}
                    </p>
                    <?php
                    }?>
                </div>
            </div>
            <div class="col-8 cols1">
                <div class="row">
                    <div class="col-3">
                        <h1 class="address">
                            <?php
                            $country_code = $asset_obj[0]->country_code;
                            switch($country_code){
                            case 1:
                            ?>
                            USA
                            <?php
                            break;
                            case 2:
                            ?>
                            Canada
                            <?php
                            break;
                            case 3:
                            ?>
                            Israel
                            <?php
                            break;
                            }
                            ?>
                        </h1>
                        <i class="fas fa-check-circle"></i>
                    </div>
                    <div class="col-9">
                        <button class="btn btn-primary btn11" data-id="{{$asset_obj[0]->id}}">
                            <?php
                            switch (Auth::user()->roles) {
                                case 1:
                                    echo "Follow";
                                    break;
                                case 2:
                                    echo "Update";
                                    break;
                            }
                            ?>
                        </button>
                        <button class="btn btn-primary btn2" id="deletebtn">
                            <?php
                            switch (Auth::user()->roles) {
                            case 1:
                            ?>
                            <i class="fas fa-caret-down"></i>
                            <?php
                            break;
                            case 2:
                                echo "Delete";
                                break;
                            }
                            ?>
                        </button>
                        <span class="dashboard font-source-sans-pro">
                            <a href="/home">
                                go to dashboard
                                <i class="fas fa-tachometer-alt"></i>
                            </a>
                        </span>
                    </div>
                </div>
                <div class="row rows2">
                    <div class="col-4">
                        <p>
                            2919 posts
                        </p>
                    </div>
                    <div class="col-4">
                        <p>
                            463k followers
                        </p>
                    </div>
                    <div class="col-4">
                        <p>
                            400 following
                        </p>
                    </div>
                </div>
                <div class="row rows4">
                    <div class="col-8">
                        <p>
                            <?php echo $asset_obj[0]->asset_description ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row rows10">
            <div class="table1">
                <div class="row">
                    <div class="col col11" id="price_div1">
                        <span>Monthly rent price:</span>
                        <div class="price1" id="rent_price">
                            {{$asset_obj[0]->price}}
                            <span>
                                <?php
                                $price_currency = $asset_obj[0]->price_currency;
                                switch($price_currency){
                                case 1:
                                ?>
                                &#36;
                                <?php
                                break;
                                case 2:
                                ?>
                                &#8362;
                                <?php
                                break;
                                case 3:
                                ?>
                                &#8364;
                                <?php
                                break;
                                }
                                ?>
                            </span>
                        </div>
                    </div>
                    <div class="col col12" id="price_div2">
                        <span>Monthly Property tax:</span>
                        <div class="price1" id="tax_price">
                            {{$asset_obj[0]->property_tax}}
                            <span>
                                <?php
                                $tax_currency = $asset_obj[0]->tax_currency;
                                switch($tax_currency){
                                case 1:
                                ?>
                                &#36;
                                <?php
                                break;
                                case 2:
                                ?>
                                &#8362;
                                <?php
                                break;
                                case 3:
                                ?>
                                &#8364;
                                <?php
                                break;
                                }
                                ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row rows105" style="margin: -25px 0 0 0">
            <div class="table2">
                <div class="row" style="height: 100%">
                    <div class="col-2">
                        <span id="snales_rating_headline">
                            Apartment rating:
                        </span>
                        <div class="other_param" style="margin: -9px 0 0 -52px" id="snales_rating_score">
                            <?php
                            $rating_from_db = $asset_obj[0]->rating;
                            $number_of_halfs_in_rating = $rating_from_db / 0.5;
                            $number_of_full_snales = floor($number_of_halfs_in_rating / 2);
                            // there are 1 or 0 hlf snales
                            $number_of_half_snales = $number_of_halfs_in_rating % 2;
                            // drow full snales
                            for ($i = 0; $i < $number_of_full_snales;$i++){
                            ?>
                            <span style="margin: 0 0 0 -5px">
                                <img src="{{ asset('img/snale_wo_slo.png') }}" alt="" style="max-width: 35px">
                            </span>
                            <?php
                            }
                            for ($j = 0;$j < $number_of_half_snales;$j++){
                            ?>
                            <span style="margin: 0 0 0 -5px">
                                <img src="{{ asset('img/snale_png_with_out_slogan_cropped.png') }}" alt=""
                                     style="max-width: 16px">
                            </span>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col" id="number_of_rooms_div">
                       <span>
                            Number of rooms:
                       </span>
                        <div class="other_param" id="number_of_rooms">
                            {{$asset_obj[0]->num_rooms}}
                        </div>
                    </div>
                    <div class="col-2" id="asset_area_div">
                        <span style="padding: 0 0 0 15%">
                            Asset area:
                        </span>
                        <div class="other_param" id="asset_area">
                            {{$asset_obj[0]->asset_area}}
                            <span>
                                <?php
                                $unit_of_measure = $asset_obj[0]->unit_of_measure;
                                switch ($unit_of_measure) {
                                    case 1:
                                        echo 'sq feet';
                                        break;
                                    case 2:
                                        echo 'sq meter';
                                        break;
                                }
                                ?>
                            </span>
                        </div>
                    </div>
                    <div class="col" id="number_of_floor_div">
                        <span>
                            Floor number:
                       </span>
                        <div class="other_param" id="number_of_floor">
                            {{$asset_obj[0]->floor_number}}
                        </div>
                    </div>
                    <div class="col" id="is_occupied_div">
                        <span style="padding: 0 0 0 7%">
                            Available:
                       </span>
                        <div class="other_param" id="is_occupied">
                            <?php
                            $available_immediately = $asset_obj[0]->available_immediately;
                            switch($available_immediately){
                            case 0:
                            ?>
                            <i class="fas fa-times"></i>
                            <?php
                            break;
                            case 1:
                            ?>
                            <i class="fas fa-check"></i>
                            <?php
                            break;
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col" id="has_porch_div">
                        <span>
                            Has porch:
                       </span>
                        <div class="other_param" id="has_porch">
                            <?php
                            $has_porch = $asset_obj[0]->hasPorch;
                            switch($has_porch){
                            case 0:
                            ?>
                            <i class="fas fa-times"></i>
                            <?php
                            break;
                            case 1:
                            ?>
                            <i class="fas fa-check"></i>
                            <?php
                            break;
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col" id="has_parking_div">
                        <span>
                            Has parking:
                       </span>
                        <div class="other_param" id="has_parking">
                            <?php
                            $has_parking = $asset_obj[0]->hasParking;
                            switch($has_parking){
                            case 0:
                            ?>
                            <i class="fas fa-times"></i>
                            <?php
                            break;
                            case 1:
                            ?>
                            <i class="fas fa-check"></i>
                            <?php
                            break;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row rows11">
            <div class="col-md-8 offset-2">
                <!-- parsing asset pictures from db -->
                <?php
                $asset_photos_objs = Photo::where('asset_id', $asset_id)->get();
                $asset_photos_array_size = sizeof($asset_photos_objs);
                $img_counter = 1;
                ?>
                <div class="row">
                    <?php
                    foreach ($asset_photos_objs as $asset_photos_obj) {
                    $img_url = $asset_photos_obj->file_name_and_path;
                    // after 3 pics in first row, if there is pic 4,5 open new row
                    if ($img_counter > 3 && $asset_photos_array_size >= $img_counter){
                    ?>
                    <div class="row" style="margin: 15px 0 0 0">
                        <?php
                        for ($j = 0;$j < max($asset_photos_array_size - 3, 2);$j++){
                        ?>
                        <div class="col-6">
                            <a href="/">
                                <img src="{{url($img_url)}}" alt=""
                                     style="margin: 0 68px; width: 200px; max-height: 150px">
                            </a>
                        </div>
                        <?php
                        $asset_photos_obj = $asset_photos_objs[$img_counter + $j - 1];
                        $img_url = $asset_photos_obj->file_name_and_path;
                        }
                        ?>
                    </div>
                    <?php
                    break;
                    }
                    ?>
                    <div class="col-4">
                        <a href="/">
                            <img src="{{url($img_url)}}" alt="" style="width: 200px; max-height: 150px">
                        </a>
                    </div>
                    <?php
                    $img_counter++;
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Snale animation + overlay -->
    <?php define('DIR1', realpath(__DIR__ . '/../../public'));
    require_once DIR1 . 'snl_svg_normal_spinner.php';
    ?>
    <!-- Animation transition overlay -->
    <div class="overlay"></div>
    <!-- -->
@endsection