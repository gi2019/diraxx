@extends('layouts.app')

@section('content')
    <div class="container-fluid upload">
        <div class="row rowu1">
            <div class="col-md-8 offset-2">
                <h1>
                    Upload your asset
                </h1>
            </div>
            <div class="dashboard font-source-sans-pro">
                <a href="/home">
                    go to dashboard
                    <i class="fas fa-tachometer-alt"></i>
                </a>
            </div>
        </div>
    @csrf
    <div id="countryStateSelect">
    <upload-asset-form-full userid="{{Auth::user()->id}}"></upload-asset-form-full>

    </div>

    <script src="{{ asset('js/app.js') }}">

    </script>

        <script src="https://maps.googleapis.com/maps/api/place/autocomplete/json?input=1600+Amphitheatre&key=AIzaSyBIi4GdjJYFX-Q4ztW0KpYMT0KJmKkHyc0">

        </script>
    </div>
@endsection