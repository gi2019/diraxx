@extends('layouts.app')

@section('content')
    <div class="row blackrow">
        <h1>
            Sign Up
        </h1>
    </div>
    <button class="btn btn-info question">
        <span class="q1">
            Question?
        </span>
        <span class="q2">
            visit our Help Center
        </span>
    </button>
    <div class="row justify-content-left">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="d1">
                        {{ __('Discover deals in Real Estate') }}
                    </div>
                    <div class="d2">
                        {{ __('The go-to destination for discovering and connecting
                        with property owners, renters and Investors.') }}
                    </div>
                    <div class="row">
                        <div class="col">
                            <button class="btn btn-info regbtn regbtn1">
                                <i class="fab fa-twitter"></i>Sign up with Twitter
                            </button>
                        </div>
                        <div class="col">
                            <button class="btn btn-success regbtn regbtn2">
                                <i class="fab fa-facebook-f"></i>Sign up with Facebook
                            </button>
                        </div>
                        <div class="col">
                            <button class="btn btn-success regbtn regbtn3">
                                <i class="fab fa-google"></i>Sign up with Google
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-form-label text-md-left">{{ __('Name') }}</label>
                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                       name="name" value="{{ old('name') }}" >

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email"
                                   class="col-md-3 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                       name="email" value="{{ old('email') }}" >
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-3 col-form-label text-md-left">{{ __('Password') }}</label>
                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password_confirm" class="col-md-3 col-form-label text-md-left">{{ __('Confirm Password') }}</label>
                            <div class="col-md-8">
                                <input id="password_confirm" type="password" name="password_confirmation"  class="form-control @error('phone_number') is-invalid @enderror" >
                                @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <!-- Additional fields required for registration -->
                        <div class="form-group row">
                            <label for="phone_number" class="col-md-3 col-form-label text-md-left">Phone Number</label>
                            <div class="col-md-8">
                                <input id="phone_number" type="tel" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number">
                                @error('phone_number')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="role" class=" col-form-label text-md-left">Register As</label>
                                </div>
                                <div class="col-md-8">
                                    <select class="input-medium select1" id="role" name="role">
                                        <option value=1>Renter</option>
                                        <option value=2>Asset Owner</option> <!-- 1 = Renter , 2 = Asset Owner -->
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <!-- Space holder -->
                            </div>
                            <div class="col-md-8">
                                <button type="submit" class="btn btn-primary submitbtn">
                                    {{ __('Create Account') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
