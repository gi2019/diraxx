<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <!-- Font Awesome -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"
            integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+"
            crossorigin="anonymous"></script>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Sanchez&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <!-- Style.css -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <title>Direxx - People meet assets meet People</title>
    <!-- JQUERY -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <!-- myscript.js -->
    <script src="http://localhost:8000/js/myScript.js"></script>
</head>
<body>
<!-- change1 -->
<div class="row row1">
    <div class="col-2 left_upper_corner">
        @if (Auth::user())
            @if (Auth::user()->email_verified_at)
                <a href="{{'/'}}">
                    <img src="{{ asset('img/snale_wo_slo.png') }}"
                         alt="" class="img1">
                    <div class="direxx1">
                        Diraxx
                    </div>
                </a>
            @else
                <a href="#">
                    <img src="{{ asset('img/snale_wo_slo.png') }}"
                         alt="" class="img1">
                    <div class="direxx1">
                        Diraxx
                    </div>
                </a>
            @endif
        @else
            <img src="{{ asset('img/snale_wo_slo.png') }}"
                 alt="" class="img1">
            <div class="direxx1">
                Direxx
            </div>
        @endif
    </div>
    <div class="col-6" id="mainsearch">
        <input type="search" class="form-control input1" @keyup="searchit" v-model="search" placeholder="Search for latest posts, assets, deals etc.">
        <i class="fas fa-search"></i>
    </div>
    <!-- Authentication Links -->
    <div class="col-4">
        @guest
            <button class="btn1 btn btn-success">Sign up</button>
            <button class="btn2 btn btn-light">Log in</button>
        @endguest
        @auth
            @if(Auth::user()->hasVerifiedEmail())
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            @else
                <button class="btn1 btn btn-success">Sign up</button>
                <button class="btn2 btn btn-light">Log in</button>
            @endif
        @endauth
    </div>
</div>
{{--<div class="container-fluid">--}}
    @yield('content')
{{--</div>--}}

@yield('footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>


</body>
</html>
