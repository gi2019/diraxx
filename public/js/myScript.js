$(document).ready(function () {
    // Ajax setup
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // sign up login will be disabled in different scenarios and/or screens
    const registerUrl = 'http://localhost:8000/register';
    const logInUrl = 'http://localhost:8000/login';
    const emailVerify = 'http://localhost:8000/email/verify';
    const dashboardUrl = 'http://localhost:8000/home';

    var btn1 = $("#btn1");
    var btn2 = $("#btn2");
    var input1 = $(".input1");

    var currentUrl = window.location.href;
    switch (currentUrl) {
        case registerUrl:
            btn1.prop('disabled', true);
            break;
        case logInUrl:
            btn2.prop('disabled', true);
            break;
        case emailVerify:
            btn1.prop('disabled', true);
            btn2.prop('disabled', true);
            input1.prop('disabled', true);
            break;
        default:
            btn1.prop('disabled', false);
            btn2.prop('disabled', false);
    }
    // Other wise route
    btn1.click(function () {
        window.location.href = registerUrl
    });

    btn2.click(function () {
        window.location.href = logInUrl
    });

    $("#snl_svg_normal").animate({
        marginLeft: '70'
    }, 3500, function () {
        var count = 0; // i want to call this function 6 times
        var refreshIntervalId = setInterval(function () {
            $("#snl_svg_blink").toggle(); // Snale blinks by show and hide
            count++;
            if (6 === count) {
                clearInterval(refreshIntervalId); // end of js loop
                $("#snl_svg_normal").hide();
                $("#snl_svg_got_idea").show();

                // delay 1 sec and than hide snale with idea, show snale that runs
                delay_running_snale();
            }
        }, 450)
    });

    /*
        Upload asset validation messages
     */
    // Turn on locality instruction message
    var local_message = $(".locality_message");
    if(!local_message.is(":visible")) {
        local_message.fadeIn(4000);
    }
    // Turn on locality instruction message
    var address_message = $(".address_message");
    if(!address_message.is(":visible")) {
        address_message.fadeIn(4000);
    }

    // Turn on locality instruction message
    var asset_description_message = $(".asset_description");
    if(!asset_description_message.is(":visible")) {
        asset_description_message.fadeIn(4000);
    }
    // Turn on picture instruction message
    var msg = $("#picture_message");
    if (!msg.is(":visible")) {
        msg.fadeIn(4000);
    }

    // submission grren button in page
    var button = $("#upload_asset_submit_btn");
    button.prop('disabled', true);
    // check two conditioned for enabling submit btn
    var is_ok_to_enable_button = 0;

    var locality = $("#locality");
    var address = $("#address");

    locality.blur(function () {
        if (locality.val() !== '' && locality.val().length > 1 && address.val() !== '' && address.val().length > 4) {
            is_ok_to_enable_button = 1;
            if (is_ok_to_enable_button) {
                button.prop('disabled', false);
            } else {
                button.prop('disabled', true);
            }
        } else {
            is_ok_to_enable_button = 0;
            button.prop('disabled', true);
        }
    })
    address.blur(function () {
        if (locality.val() !== '' && locality.val().length > 1 && address.val() !== '' && address.val().length > 4) {
            is_ok_to_enable_button = 1;
            if (is_ok_to_enable_button) {
                button.prop('disabled', false);
            } else {
                button.prop('disabled', true);
            }
        } else {
            is_ok_to_enable_button = 0;
            button.prop('disabled', true);
        }
    })

    $('form').submit(function (e) {
        e.preventDefault();
        // Show overlay div
        $(".overlay").show();
        // Show normal snale
        $(".snl_svg_normal_spinner").show();
        // Normal snale moves
        $("#snl_svg_normal_spinner").animate({
            marginLeft: '70'
        }, 4000);
        var form = this;
        // Submit form
        setTimeout(function () {
            form.submit();
        }, 4000);
    });
    // Display the name of the files
    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    /*
    * Upload Asset data process with Ajax
    * */
    // Text area
    var asset_decription_ancestor1 = $(".rows4 .col-8");
    var asset_description = asset_decription_ancestor1.first().text().trim();
    $(".btn11").click(function () {
        // SPA change page to update page
        if ($(this).text()!== "Submit") {
            // Update button
            $(this).css("background-color", "green");
            $(this).css("border-color", "green");
            $(this).text("Submit");
            // Delete button
            var deletebtn = $("#deletebtn");
            $(deletebtn).css("background-color", "red");
            $(deletebtn).css("border-color", "red");
            $(deletebtn).text("Cancel");
            // Text area
            asset_decription_ancestor1.append("<textarea id='asset_description'>" + asset_description + "</textarea>");
            $(".rows4 .col-8 p").remove();
            // Circle two inputs
            var asset_address_p_ancestor1 = $(".circ1");
            var asset_address_p = $(".circ1 p");
            var text1 = asset_address_p.text().trim();

            var locality = $(".circText2");
            var text2 = locality.text().trim();

            asset_address_p_ancestor1.append("<input type='text' id='address' value = '"+ text1 +"' style='max-width: 120px;max-height: 20px;margin: 28% 10% 0 '>");
            asset_address_p_ancestor1.append("<input type='text' id='locality' value = '"+ text2 +"' style='max-width: 100px;max-height: 20px;margin: 18% 16% 0 '>");
            asset_address_p.remove();
            locality.remove();
            // price1
            var rent_price_ancestor = $("#price_div1");
            var rent_price_obj = $("#rent_price");
            var rent_price_text = rent_price_obj.text().replace(/\D/g, '');
            rent_price_ancestor.append("<input type='number' value='" + rent_price_text + "' id='rent_price' style='max-width: " +
                "80px;margin: 20px 10px 0 8px;padding: 0 0 0 8%;'>");
            // append rent currency
            rent_price_ancestor.append("<select id='price_currency' style='height: 28px; border: 0 solid lightgrey'>" +
                "  <option value=1>&#36;</option>\n" +
                "  <option value=2>&#8362;</option>\n" +
                "  <option value=3>&#8364;</option>\n" +
                "</select>");
            rent_price_obj.remove();
            // price2
            var monthly_property_tax_ancestor = $("#price_div2");
            var monthly_property_tax_obj = $("#tax_price");
            var monthly_property_tax_text = monthly_property_tax_obj.text().replace(/[^0-9]/g, '');
            monthly_property_tax_ancestor.append("<input type='number' value='" + monthly_property_tax_text + "' id='monthly_property_tax' style='max-width: " +
                "70px;margin: 20px 10px 0 8px;padding: 0 0 0 6%;'>");
            // append tax currency
            monthly_property_tax_ancestor.append("<select id='tax_currency' style='height: 28px; border: 0 solid lightgrey'>" +
                "  <option value=1>&#36;</option>\n" +
                "  <option value=2>&#8362;</option>\n" +
                "  <option value=3>&#8364;</option>\n" +
                "</select>");
            monthly_property_tax_obj.remove();
            //col_snales_rating remove headline and score
            var snales_rating_headline = $("#snales_rating_headline");
            snales_rating_headline.remove();
            var snales_rating_score = $("#snales_rating_score");
            snales_rating_score.remove();
            //number of rooms
            var number_of_rooms_obj = $("#number_of_rooms_div");
            var number_of_rooms = $("#number_of_rooms");
            var number_of_rooms_text = number_of_rooms.text().trim();
            number_of_rooms_obj.append("<input type='number' id='number_of_rooms' value='" + number_of_rooms_text + "' style='max-width:70px;padding:2px 0 0 14%;margin: 5px 6px'>");
            number_of_rooms.remove();
            //asset area
            var asset_area_obj = $("#asset_area_div");
            var asset_area = $("#asset_area");
            var asset_area_text = asset_area.text().replace(/[^0-9]/g, '');
            asset_area_obj.append("<input type='number' id='asset_area' value='" + asset_area_text + "' style='max-width:80px;padding:2px 0 0 18%;margin: 5px 4px'>");
            asset_area.remove();
            // unit of meassure
            asset_area_obj.append("<select id='asset_area_unit_of_measure' style='width:50px;height: 24px; font-size: 10px ;border: 0 solid lightgrey'>" +
                "  <option value=1> feet^2</option>\n" +
                "  <option value=2>meter^2</option>\n" +
                "</select>");
            // floor number
            var floor_number_obj = $("#number_of_floor_div");
            var floor_number = $("#number_of_floor");
            floor_number_text = floor_number.text().trim();
            floor_number_obj.append("<input type='number' id='floor_number' value='" + floor_number_text + "' style='max-width:60px;padding:2px 0 0 20%;margin: 5px 8px'>");
            floor_number.remove();
            // is_occupied
            var is_occupied_obj = $("#is_occupied_div");
            var is_occupied = $("#is_occupied");
            is_occupied_obj.append("<select id='is_occupied' style='margin: 5px 9px; height: 28px; border: 0 solid lightgrey'>" +
                "  <option value=0>No</option>" +
                "  <option value=1>Yes</option>" +
                "</select>");
            is_occupied.remove();
            // has_porch
            var has_porch_obj = $("#has_porch_div");
            var has_porch = $("#has_porch");
            has_porch_obj.append("<select id='has_porch' style='margin: 5px 17px 0 0; height: 28px; border: 0 solid lightgrey'>" +
                "  <option value=0>No</option>" +
                "  <option value=1>Yes</option>" +
                "</select>");
            has_porch.remove();
            // has_parking
            var has_parking_obj = $("#has_parking_div");
            var has_parking = $("#has_parking");
            has_parking_obj.append("<select id='has_parking' style='margin: 5px 5px; height: 28px; border: 0 solid lightgrey'>" +
                "  <option value=0>No</option>" +
                "  <option value=1>Yes</option>" +
                "</select>");
            has_parking.remove();
        } else{
            // Submit
            $.ajax({
                type:'PUT',
                url: '/submitUpdatbleData',
                data: {
                    asset_id : $(".btn11").data("id"),
                    asset_description : $("#asset_description").val(),
                    address : $("#address").val(),
                    locality : $("#locality").val(),
                    rent_price : $("#rent_price").val(),
                    price_currency : $("#price_currency").val(),
                    monthly_property_tax : $("#monthly_property_tax").val(),
                    tax_currency : $("#tax_currency").val(),
                    number_of_rooms : $("#number_of_rooms").val(),
                    asset_area : $("#asset_area").val(),
                    asset_area_unit_of_measure : $("#asset_area_unit_of_measure").val(),
                    floor_number : $("#floor_number").val(),
                    available_immediately : $("#is_occupied").val(),
                    has_porch : $("#has_porch").val(),
                    has_parking : $("#has_parking").val()
                },
                dataType: "TEXT",
                success: function(response){
                    console.log(response);
                    $(".overlay").show();
                    // Show normal snale
                    $(".snl_svg_normal_spinner").show();
                    // Normal snale moves
                    $("#snl_svg_normal_spinner").animate({
                        marginLeft: '110'
                    }, 4300);
                    // Reroute
                    setTimeout(function () {
                        window.location.href = dashboardUrl
                    }, 4300);
                }
            });
        }
    })
    /*
     * Delete , cancel update process
     */
    $("#deletebtn").click(function(){
        // button submit back to update
        var btn11 = $(".btn11");
        btn11.css("background-color", "#369AF2");
        btn11.css("border-color", "#369AF2");
        btn11.text("Update");

        $("#asset_description").remove();
        asset_decription_ancestor1.append("<p>"+asset_description+"</p>");
    })

});
var flag = 0;
// function that delays action by 1 sec than executes.
var delay_running_snale = function () {
    if (flag) {
        $("#snl_svg_got_idea").hide();
        var snl_svg_runs = $("#snl_svg_runs");
        snl_svg_runs.show();
        snl_svg_runs.animate({
                marginLeft: '170'
            }, 1250,
            function () {
                var delay_snale_enter_home_after_running = setTimeout(function () {
                    $("#snl_svg_runs").hide();
                    $("#snale_enters_his_house").show();
                    $("#house_of_snale_svg_open_door").show();
                    var snale_entered_door_slammed = setTimeout(function () {
                        $("#snale_enters_his_house").hide();
                        $("#house_of_snale_svg_open_door").hide();
                    }, 1500)
                }, 1500);
            });
    }
    else {
        flag = 1;
        setTimeout(delay_running_snale, 2500); // delay_running_snale again in a second
    }
}
